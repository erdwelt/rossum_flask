import json
import requests


class RossumAPICommunication:
    """Class for handling communication with RossumAPIs"""

    def __init__(self):
        self.user_details = self._get_user_details()
        self.auth_key = self._get_auth_key()
        self.auth_key_string = self._auth_key_to_dict()
        self.connected = self._is_authenticated()

    # Private methods
    def _get_auth_key(self):
        """ Getting authentication token for communication. """
        try:
            p = requests.post('https://api.elis.rossum.ai/v1/auth/login', data=self.user_details)
            return json.loads(p.text)['key']
        except requests.exceptions.RequestException as err:
            print(err)
        except Exception as err:
            print(err)

    @staticmethod
    def _get_user_details():
        """ Simulates login access details. """
        with open('login_details.txt', 'r') as file:
            for line in file.readlines():
                _list = line.rstrip('\n').split(',')
                return {'username': _list[0], 'password': _list[1]}

    def _is_authenticated(self):
        """Using documentation token for length comparison of connectedness.
        More advanced function could be passed here."""
        if len(str(self.auth_key)) == len('db313f24f5738c8e04635e036ec8a45cdd6d6b03'):  # Documentation
            self.connected = True
            return self.connected
        else:
            self.connected = False
            return self.connected

    def _auth_key_to_dict(self):
        """ Formats authentication token. """
        return {'Authorization': f'token {self.auth_key}'}

    def logout(self):  # Does POST request to this address always return {"detail":"Successfully logged out."} ???
        """ Sends POST request to log out with authentication token. """
        p = requests.post('https://api.elis.rossum.ai/v1/auth/logout', headers=self.auth_key_string)
        return p.text

    def extracted_data(self, queue_ID, annotation_ID):
        """ Sends GET request to get not formatted xml string with queueID and annotationID as parameters. """

        # In documentation is 'status':'exported', but if I haven't manually exported the annotation
        # the status was set to 'confirmed', which than it couldn't find it.
        # Without 'status' parameters I have obtained same results.
        payload = {'format': 'xml',
                   'id': annotation_ID}
        # payload['status'] = 'exported' # Commented out as it's not needed.
        try:
            r = requests.get(f'https://api.elis.rossum.ai/v1/queues/{queue_ID}/export', params=payload,
                             headers=self.auth_key_string)
            if str(r.status_code)[0] == '2':  # Check for status code starting with 200
                return r.text
        except requests.exceptions.ConnectionError as err:
            print('Connection error:', err)
        except requests.exceptions.RequestException as err:
            print('Request exception error:', err)
        except Exception as err:
            print('Something went wrong, general exception.', err)

    @staticmethod
    def send_xml(annotationID, base64string):
        """ Sends parsed data to dummy URL. """

        payload = {"annotationID": annotationID,
                   "content": base64string}
        try:
            r = requests.post(r'https://my-little-endpoint.ok/rossum', data=json.dumps(payload))
            if str(r.status_code)[0] == '2':  # Check for status code starting with 2**
                return json.dumps({"success": "true"})
            else:
                return json.dumps({"success": "false"})
        except requests.exceptions.ConnectionError:  # Catches connection timeout from non-responding dummy URL.
            return json.dumps({"success": "false"})
        except Exception as err:
            print('Something went wrong, general exception.', err)

    """ Three methods used in development phase but could be useful for more advanced class. """
    def __get_annotation_list(self):
        """ Not needed for final product. Used in development phase. """
        try:
            r = requests.get('https://api.elis.rossum.ai/v1/annotations', headers=self.auth_key_string)
            if str(r.status_code)[0] == '2':
                return json.loads(r.text)
        except requests.exceptions.RequestException as err:
            print(err)

    @staticmethod
    def __get_annotationID(json_string):
        """ Extracts annotationID from full annotation list. Used in development phase. """

        # The 0 there is problematic if more than sample document are present
        # Also, why is the /v1/documents/ID different from 'id': id??
        return json_string['results'][0]['id']

    @staticmethod
    def __get_queueID(json_string):
        """ Extracts queueID from full annotation list. Used in development phase. """
        queue_url = json_string['results'][0]['queue']  # Unlike json_string['results'][0]['id'] returns full URL.
        ret_list = []
        for char in reversed(queue_url):
            if char == '/':
                break
            else:
                ret_list.append(char)
        return int(''.join(reversed(ret_list)))  # Returns just queueID


if __name__ == '__main__':
    pass
