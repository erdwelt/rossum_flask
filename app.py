import werkzeug.exceptions
from flask import Flask
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
import os
import rossumAPIClass
import xmltodict  # It's downloaded XML parser, but doesn't seem to work well as a library.
import base64


app = Flask(__name__)
auth = HTTPBasicAuth()

def check_env_var():  # Currently, very low encryption protection for password checking, keeping it in different file.
    """ Gets environmental variables """
    username = os.environ.get('USERNAME_flask')
    password = os.environ.get('PASSWORD_flask')

    # Compares with stored usernames and passwords
    with open('password.txt', 'r') as file:
        for line in file.readlines():
            _list = line.rstrip('\n').split(',')
            if _list == [username, password]:
                return {username: generate_password_hash(password)}
    return {}


@auth.verify_password
def verify_password(username, password):
    user = check_env_var()
    if username not in user:
        print("Send the request again from verified user.")
    if username in user and check_password_hash(user.get(username), password):
        return username
    return False


@app.route('/export/<annotationID>/<queueID>', methods=['POST'])
@auth.login_required
def get_data(annotationID=0, queueID=0):
    # Initializing
    returning = "Something went wrong if you are seeing this."
    rossum = rossumAPIClass.RossumAPICommunication()

    if rossum.connected is True:  # Printing on server side i.e. docker logs.
        print('Requested annotationID is: ', str(annotationID))
        print('Requested queueID is: ', str(queueID))

        xml_data = rossum.extracted_data(queueID, annotationID)
        if xml_data is None:
            raise werkzeug.exceptions.BadRequest

        data_dict = xmltodict.parse(xml_data.replace("\n", ""))  # Getting rid of the escape characters while parsing.
        xml_string = xmltodict.unparse(data_dict, pretty=True)  # Parsing it correctly and allowing for pretty print
        # print(xml_string) # Just for docker console print

        b64_safe = base64.urlsafe_b64encode(xml_string.encode('ascii')).decode('ascii')  # URL safe b64.

        returning = rossum.send_xml(annotationID, b64_safe)

    print(rossum.logout())
    return returning


if __name__ == '__main__':
    app.run()
