Prvně děkuji za Váš čas a zvážení mě.

Přidal jsem Powershell ISE script, který by to měl spustit a udělat iniciální test a PS vytisknout, jestli POST byl úspěšný nebo ne a jestli úspěšně byli POSTnuté data na dummy https://my-little-endpoint.ok/rossum.
Což jestli jsem pochopil správně by vždy mělo být false, protože to není endpoint schopný přijímat requesty.


Pár poznámek.
1) Nepodařilo se mi připojit STDIN pro new_post.py, takže to neběží z Powershellu. Což je mi líto, ale někde mi chyběl malý kousek znalostí, na který bych se normálně zeptal zkušenějšího kolegy.
Zároveň ale, když se vstoupí do docker containeru matyas_container tak se to tam dá spustit skrze command line a vše funguje jak by mělo.
Je potřeba v FlaskDockerbuild.ps1 změnit adresář v prvním řádku.
2) Je tam možnost odkomentovat xml_pretty print na potvrzení správného parsování xml dokumentu.
3) xmltodict.py není můj výtvor, ale zároveň mi to nefungovalo jako samostatná library, protože tomu chyběli základní __init__.py a další soubory, aby to mohl být module, takže jsem to dal natvrdo do stejného adresáře.
4) v RossumAPIClass.py jsem nechal několik method, které jsem používal v testovací fázy, myslím si, že by mohly být užitečné, kdyby se s tímto pracovalo dále, tak jsem je nechal jako příklad a udělal z nich privátní metody (tedy tak moc jak to python povolí aby byly skryté.)
5) Veškerá hesla jsou uložené v adresáři, ale říkal jsem si, že nejsou vidět nikde v API pointu, takže kdyby se někdo dostal do adresáře, tak by to byl asi větší problém, než to, že nejsou zašifrované.
6) Natvrdo nastavuji enviromental variables v Dockerfile, ale vždy to ověřuje a měli by se dát i přímo ze skriptu přenastavit, pokud nesedí s uživatelem a heslem ze zadání.
7) Vrací https://api.elis.rossum.ai/v1/auth/logout vždy {"detail":"Successfully logged out."}? Protože i když jsem tam posílal POST bez jakéhokoliv auth tokenu tak mi to vracelo tuto zprávu.
To se mi zdá špatně nastavené, že neověřuje, jestli token existuje nebo ne.
