# syntax=docker/dockerfile:1
FROM python:3.9.0

ENV USERNAME_flask="myUser123"
ENV PASSWORD_flask="secretSecret"

MAINTAINER Matyas "matyas@jirat.cz"

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip3 install -r requirements.txt

COPY . /app

CMD ["python3","-m","app","run","--host=127.0.0.1"]
