import requests
import os
import sys
import werkzeug.exceptions


def add_env_var():
    """ Adds environmental variables """
    print("Input USERNAME_flask env variable: ")
    os.environ['USERNAME_flask'] = str(sys.stdin.readline().rstrip('\n'))
    print("Input PASSWORD_flask env variable: ")
    os.environ['PASSWORD_flask'] = str(sys.stdin.readline().rstrip('\n'))


def check_env_var():
    """ Gets environmental variables """
    username = os.environ.get('USERNAME_flask')
    password = os.environ.get('PASSWORD_flask')
    return username, password


def details_read(file_location):
    """ Compares with stored usernames and passwords, returns tuple"""
    username = os.environ.get('USERNAME_flask')
    password = os.environ.get('PASSWORD_flask')

    with open(file_location, 'r') as file:
        for line in file.readlines():
            _list = line.rstrip('\n').split(',')
            if _list == [username, password]:
                return username, password
    return False, False


def test():
    """ Runs a customized POST request """

    print("Enter customized POST request to Rossum API")

    user_pass_tuple = check_env_var()
    if user_pass_tuple != details_read('password.txt'):
        print("User not verified, please enter correct log in details.")
        try:
            add_env_var()
        except EOFError as err:
            print("Interactive shell not opened.\n", err)
        finally:
            user_pass_tuple = check_env_var()

    try:
        print("Enter annotationID: ")
        annotationID = int(sys.stdin.readline())
        print("Enter queueID")
        queueID = int(sys.stdin.readline())
        p = requests.post(f'http://127.0.0.1:5000/export/{annotationID}/{queueID}', auth=user_pass_tuple)
        if str(p.status_code)[0] == '2':
            print('POST request successfully posted.')
            print("Whether the data was posted to dummy URL: ", p.text)
            print("Always expected false due to it not receiving data.")
        else:
            print('POST request was unsuccessful.')
    except ValueError:
        print("Accepts only integers for ID values.")
    except werkzeug.exceptions.BadRequest as err:
        print('Error 400:', err)
    except Exception as err:
        print('Something went wrong, general exception ', err)


if __name__ == '__main__':
    test()
