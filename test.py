import requests
import os
from getpass import getpass

import werkzeug.exceptions


def add_env_var():
    """ Adds environmental variables """
    os.environ['USERNAME_flask'] = str(input("Input USERNAME_flask env variable: "))
    os.environ['PASSWORD_flask'] = str(getpass("Input PASSWORD_flask env variable: "))


def check_env_var():
    """ Gets environmental variables """
    username = os.environ.get('USERNAME_flask')
    password = os.environ.get('PASSWORD_flask')
    return username, password


def details_read(file_location):
    """ Compares with stored usernames and passwords, returns tuple"""
    username = os.environ.get('USERNAME_flask')
    password = os.environ.get('PASSWORD_flask')

    with open(file_location, 'r') as file:
        for line in file.readlines():
            _list = line.rstrip('\n').split(',')
            if _list == [username, password]:
                return username, password
    return False, False


def test():
    """ Runs a test POST request """

    print("Running a test POST request to Rossum API")

    user_pass_tuple = check_env_var()
    if user_pass_tuple != details_read('password.txt'):
        print("User not verified, please enter correct log in details.")
        try:
            add_env_var()
        except EOFError as err:
            print("Interactive shell not opened.\n", err)
        finally:
            user_pass_tuple = check_env_var()

    try:
        p = requests.post(r'http://127.0.0.1:5000/export/11617833/164460', auth=user_pass_tuple)
        if str(p.status_code)[0] == '2':
            print('POST request successfully posted.')
        print("Whether the data was posted to dummy URL: ", p.text)
        print("Always expected false due to it not receiving data.")
    except werkzeug.exceptions.Forbidden as err:
        print('Unauthorized access, Error code 403.\n', err)
    except requests.exceptions.ConnectionError as err:
        print('Connection error:', err)
    except requests.exceptions.RequestException as err:
        print('Request exception error:', err)
    except Exception as err:
        print('Something went wrong, general exception.', err)


if __name__ == '__main__':
    test()
